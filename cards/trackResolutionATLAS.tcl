set PResolutionFormula {
    ( abs(eta) > 0.1 && abs(eta) <= 0.7 ) * pt * pt * 0.021 +\
    ( abs(eta) > 0.7 && abs(eta) <= 1.2 ) * pt * pt * 0.031 +\
    ( abs(eta) > 1.2 && abs(eta) <= 2.0 ) * pt * pt * 0.048 +\
    ( abs(eta) > 2.0 && abs(eta) <= 2.2 ) * pt * pt * 0.059
}
set CtgThetaResolutionFormula { 0.0 }

# taken from ATLAS-TDR-029 tab. 13.8
set PhiResolutionFormula {
    ( abs(eta) > 0.1 && abs(eta) <= 0.7 ) * 0.003 +\
    ( abs(eta) > 0.7 && abs(eta) <= 1.2 ) * 0.003 +\
    ( abs(eta) > 1.2 && abs(eta) <= 2.0 ) * 0.013 +\
    ( abs(eta) > 2.0 && abs(eta) <= 2.2 ) * 0.012
}
set D0ResolutionFormula {
    ( abs(eta) > 0.1 && abs(eta) <= 0.7 ) * 0.42 +\
    ( abs(eta) > 0.7 && abs(eta) <= 1.2 ) * 0.52 +\
    ( abs(eta) > 1.2 && abs(eta) <= 2.0 ) * 0.87 +\
    ( abs(eta) > 2.0 && abs(eta) <= 2.2 ) * 1.03
}
set DZResolutionFormula {
    ( abs(eta) > 0.1 && abs(eta) <= 0.7 ) * 2.9 +\
    ( abs(eta) > 0.7 && abs(eta) <= 1.2 ) * 4.5 +\
    ( abs(eta) > 1.2 && abs(eta) <= 2.0 ) * 19.3 +\
    ( abs(eta) > 2.0 && abs(eta) <= 2.2 ) * 22.1
}
