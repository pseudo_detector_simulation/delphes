#include "TLorentzVector.h"
#include "fastjet/newJetVariables.hh"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"
#include "TMatrixD.h"
#include "TDecompSVD.h"
#include <math.h>

using namespace std;

FASTJET_BEGIN_NAMESPACE

 vector<PseudoJet> boostToCenterOfMass(PseudoJet jet,vector<PseudoJet> constit_pseudojets) {
 vector<PseudoJet> clusters;
 if(jet.e() < 1e-20) { // FPE
   return clusters;
	  }

	  double bx = jet.px()/jet.e();
	  double by = jet.py()/jet.e();
	  double bz = jet.pz()/jet.e();

	  if(bx*bx + by*by + bz*bz >= 1) { // Faster than light
	    return clusters;
	  }

	  for(unsigned int i1=0; i1 < constit_pseudojets.size(); i1++) {
	    TLorentzVector v;
	    v.SetPxPyPzE(constit_pseudojets.at(i1).px(), constit_pseudojets.at(i1).py(),constit_pseudojets.at(i1).pz(),constit_pseudojets.at(i1).e());
	    v.Boost(-bx,-by,-bz);
	    PseudoJet v2(v.Px(), v.Py(), v.Pz(), v.E());
	    clusters.push_back(v2);
	  }

	  return clusters;
	}

float Angularity(PseudoJet &jet)
{
  if(jet.constituents().size() == 0) return -1;
  if(jet.m()==0.0) return -1;

  vector<PseudoJet> constit_pseudojets = jet.constituents();
  TLorentzVector jet_p4(jet.px(), jet.py(), jet.pz(), jet.e());

  float Angularity2=-1.;
  float m_a2=-2.;
  float sum_a2=0.;

  for(unsigned int iConstit=0; iConstit < constit_pseudojets.size(); iConstit++) {
    TLorentzVector tclus = TLorentzVector(constit_pseudojets[iConstit].px(),constit_pseudojets[iConstit].py(),constit_pseudojets[iConstit].pz(),constit_pseudojets[iConstit].e());
    float theta_i = jet_p4.Angle(tclus.Vect());
    float sintheta_i = sin(theta_i);
    if( sintheta_i == 0 ) continue; // avoid FPE
    float e_theta_i_a2 = constit_pseudojets[iConstit].E()*pow(sintheta_i,m_a2)*pow(1-cos(theta_i),1-m_a2);
    sum_a2 += e_theta_i_a2;
  }

  if ( jet.m() < 1.e-20 ) return -1.0;
  Angularity2 = sum_a2/jet.m();
  return Angularity2;
}


float PlanarFlow(PseudoJet &jet){

  float PF=-1.;
  if(jet.m() == 0.0 ) return PF;
  if(jet.constituents().size() == 0 ) return PF;

  vector<PseudoJet> constit_pseudojets = jet.constituents();

  TMatrixDSym MomentumTensor(2);
  //Planar flow
  float phi0=jet.phi();
  float eta0=jet.eta();

  float nvec[3];
  nvec[0]=(cos(phi0)/cosh(eta0));
  nvec[1]=(sin(phi0)/cosh(eta0));
  nvec[2]=tanh(eta0);

  //this is the rotation matrix
  float RotationMatrix[3][3];

  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      RotationMatrix[i][j] = 0.;
    }
  }

  float mag3 = sqrt(nvec[0]*nvec[0] + nvec[1]*nvec[1]+ nvec[2]*nvec[2]);
  float mag2 = sqrt(nvec[0]*nvec[0] + nvec[1]*nvec[1]);

  if(mag3 <= 0) {
    // Rotation axis is null
    return PF;
  }

  float ctheta0 = nvec[2]/mag3;
  float stheta0 = mag2/mag3;
  float cphi0 = (mag2>0.) ? nvec[0]/mag2:0.;
  float sphi0 = (mag2>0.) ? nvec[1]/mag2:0.;

  RotationMatrix[0][0] =- ctheta0*cphi0;
  RotationMatrix[0][1] =- ctheta0*sphi0;
  RotationMatrix[0][2] = stheta0;
  RotationMatrix[1][0] = sphi0;
  RotationMatrix[1][1] =- 1.*cphi0;
  RotationMatrix[1][2] = 0.;
  RotationMatrix[2][0] = stheta0*cphi0;
  RotationMatrix[2][1] = stheta0*sphi0;
  RotationMatrix[2][2] = ctheta0;


  for(vector<PseudoJet>::iterator cit=constit_pseudojets.begin(); cit != constit_pseudojets.end();
  cit++) {
    PseudoJet & cp = *cit;
    TLorentzVector p = TLorentzVector(cp.px(),cp.py(),cp.pz(),cp.e());
    float n=1./(cp.e()*jet.m());
    float px_rot = RotationMatrix[0][0] * (p.Px())+RotationMatrix[0][1]
    * (p.Py())+RotationMatrix[0][2]*(p.Pz());
    float py_rot = RotationMatrix[1][0] * (p.Px())+RotationMatrix[1][1]
    * (p.Py())+RotationMatrix[1][2]*(p.Pz());
    float pz_rot = RotationMatrix[2][0] * (p.Px())+RotationMatrix[2][1]
    * (p.Py())+RotationMatrix[2][2]*(p.Pz());

    TLorentzVector prot;
    prot.SetPxPyPzE(px_rot, py_rot, pz_rot, p.E() );

    MomentumTensor(0,0) += n * prot.Px() * prot.Px();
    MomentumTensor(0,1) += n * prot.Py() * prot.Px();
    MomentumTensor(1,0) += n * prot.Px() * prot.Py();
    MomentumTensor(1,1) += n * prot.Py() * prot.Py();
  }

  TMatrixDSymEigen eigen(MomentumTensor);
  TVectorD Lambda = eigen.GetEigenValues();
  float num = 4*Lambda[0]*Lambda[1];
  float den = (Lambda[0]+Lambda[1]) * (Lambda[0]+Lambda[1]);
  if ( fabs(den) < 1.e-20 ) return PF;
  PF = num/den;
  return PF;
}

float KtDeltaR(PseudoJet &jet, double m_jetrad)
{
  vector<PseudoJet> constit_pseudojets = jet.constituents();
  if ( constit_pseudojets.size() < 2 ) {
    //Jet has fewer than 2 constituents.
    return 0.0;
  }

  JetDefinition jetdef = JetDefinition(kt_algorithm, m_jetrad);
  ClusterSequence cs(constit_pseudojets, jetdef);
  vector<PseudoJet> outjets = cs.exclusive_jets(2);
  if ( outjets.size() < 2 ) {
    //"Fewer than two subjets found.
    return 0.0;
  }

  // TODO: Switch to JetUtils helpers for dR when package is migrated
  static float twopi = 2.0*acos(-1.0);
  float deta = outjets[1].eta() - outjets[0].eta();
  float dphi = abs(outjets[1].phi() - outjets[0].phi());
  if ( dphi > twopi ) dphi -= twopi;
  float dr = sqrt(deta*deta + dphi*dphi);
  return dr;
}


map<string, double> SphericityTensor(PseudoJet &jet)
{
  map<string, double> Variables;
  Variables["Sphericity"] = -999.*1000.;
  Variables["Aplanarity"] = -999.*1000.;

  vector<PseudoJet> clusters = boostToCenterOfMass(jet, jet.constituents());
  if(clusters.size() < 2) return Variables;

  TMatrixD MomentumTensor(3,3);
  double P2Sum = 0;

  for(std::vector<PseudoJet>::const_iterator Itr=clusters.begin(); Itr!=clusters.end(); Itr++) {
    MomentumTensor(0,0) += (*Itr).px()*(*Itr).px();
    MomentumTensor(0,1) += (*Itr).px()*(*Itr).py();
    MomentumTensor(0,2) += (*Itr).px()*(*Itr).pz();
    MomentumTensor(1,0) += (*Itr).py()*(*Itr).px();
    MomentumTensor(1,1) += (*Itr).py()*(*Itr).py();
    MomentumTensor(1,2) += (*Itr).py()*(*Itr).pz();
    MomentumTensor(2,0) += (*Itr).pz()*(*Itr).px();
    MomentumTensor(2,1) += (*Itr).pz()*(*Itr).py();
    MomentumTensor(2,2) += (*Itr).pz()*(*Itr).pz();

    P2Sum += (*Itr).px()*(*Itr).px()+(*Itr).py()*(*Itr).py()+(*Itr).pz()*(*Itr).pz();
  }

  double Aplanarity = -1;
  double Sphericity = -1;

  if(P2Sum > 0) {
    for(int i=0; i<3; i++) {
      for(int j=0; j<3; j++) {
        MomentumTensor(i,j) /= P2Sum;
      }
    }

    TDecompSVD * aSVD = new TDecompSVD(MomentumTensor);
    TVectorD Lambda = aSVD->GetSig();

    Aplanarity = 1.5*Lambda[2];
    Sphericity = 1.5*(Lambda[1]+Lambda[2]);

    delete aSVD;
  }
  else {
    return Variables;
  }

  Variables["Aplanarity"] = Aplanarity;
  Variables["Sphericity"] = Sphericity;
  return Variables;
}
double KtSplittingScale(PseudoJet &jet)
{
	  if(jet.constituents().size() == 0) return -1;

	  JetDefinition jet_def = JetDefinition(kt_algorithm, 1.5,
	                                                          E_scheme, Best);
	  ClusterSequence kt_clust_seq(jet.constituents(), jet_def);
	  PseudoJet kt_jet = sorted_by_pt(kt_clust_seq.inclusive_jets()).front();
	  double split = 1.5*sqrt(kt_clust_seq.exclusive_subdmerge(kt_jet, 4));
	  return split;
  }


double ZCut( PseudoJet &jet)
	{
	  vector<PseudoJet> constit_pseudojets = jet.constituents();
	  if(constit_pseudojets.size() == 0) return -1;

	  JetDefinition jet_def = JetDefinition(kt_algorithm, 1.5,
	                                                          E_scheme, Best);
	  ClusterSequence kt_clust_seq(constit_pseudojets, jet_def);

	  if(constit_pseudojets.size() < 4) {
	    //We were asked to calculate zCut, but there are not enough constituents
	    return 0.0;
	  }
	  vector<PseudoJet> subjets = kt_clust_seq.exclusive_jets((int)4.);

	  // Find last split jet (cluster_hist_index should be highest for the last created jet)
	  PseudoJet *lastSplitSubjet = NULL;
	  int max_cluster_hist_index = -1;
	  for(size_t iSubjet=0; iSubjet < subjets.size(); iSubjet++) {
	    PseudoJet parent1, parent2;
	    if(kt_clust_seq.has_parents(subjets[iSubjet], parent1, parent2) &&
	       subjets[iSubjet].cluster_hist_index() > max_cluster_hist_index) {
	      max_cluster_hist_index = subjets[iSubjet].cluster_hist_index();
	      lastSplitSubjet = &subjets[iSubjet];
	    }
	  }

	  if(lastSplitSubjet == NULL) {
	    //None of the subjets were split
	    return 0.0;
	  }

	  double dmin = pow(KtSplittingScale(jet), 2.0);

	  double zcut = -1;
	  if(dmin == 0)
	    zcut = 0;
	  else
	    zcut = dmin / (dmin + lastSplitSubjet->m2());

	  return zcut;
	}



FASTJET_END_NAMESPACE
